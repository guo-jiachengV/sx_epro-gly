import { createRouter, createWebHistory } from 'vue-router'
import r_login from './components/r_login.vue'
import home from './components/home.vue'
import supervisionData from './components/supervisionData.vue'
import supervisionDataDetails from './components/supervisionDataDetails.vue'
import supervisionDataAssign from './components/supervisionDataAssign.vue'
import griderConfirmData from './components/griderConfirmData.vue'
import griderConfirmDataDetails from './components/griderConfirmDataDetails.vue'
import provinceStatistics from './components/provinceStatistics.vue'
import AQIDistributionStatistics from './components/AQIDistributionStatistics.vue'
import AQIMonthStatistics from './components/AQIMonthStatistics.vue'
import otherStatistics from './components/otherStatistics.vue'
import homePageView from './views/HomePageView.vue'

const routes = [
  {
    path: '/',
    name: 'r_login',
    component: r_login
  },
  {
	path:'/homePageView',
	name:'homePageView',
	component: homePageView
  },
  {
    path: '/home',
    name: 'home',
	redirect: 'supervisionData',
    component: home,
	children:[
		{
			path: '/supervisionData',
			name: 'supervisionData',
			component: supervisionData
		},
		{
			path: '/supervisionDataDetails',
			name: 'supervisionDataDetails',
			component: supervisionDataDetails
		},
		{
			path: '/supervisionDataAssign',
			name: 'supervisionDataAssign',
			component: supervisionDataAssign
		},
		{
			path: '/griderConfirmData',
			name: 'griderConfirmData',
			component: griderConfirmData
		},
		{
			path: '/griderConfirmDataDetails',
			name: 'griderConfirmDataDetails',
			component: griderConfirmDataDetails
		},
		{
			path: '/provinceStatistics',
			name: 'provinceStatistics',
			component: provinceStatistics
		},
		{
			path: '/AQIDistributionStatistics',
			name: 'AQIDistributionStatistics',
			component: AQIDistributionStatistics
		},
		{
			path: '/AQIMonthStatistics',
			name: 'AQIMonthStatistics',
			component: AQIMonthStatistics
		},
		{
			path: '/otherStatistics',
			name: 'otherStatistics',
			component: otherStatistics
		}
		
	]
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router