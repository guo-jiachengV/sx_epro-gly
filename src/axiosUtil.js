//引入axios
import axios,{spread} from 'axios'
import { ElMessageBox, ElMessage,ElNotification} from 'element-plus'
//设置baseURL
axios.defaults.baseURL="http://localhost:8071"
//设置超时时间
axios.defaults.timeout=3000
axios.interceptors.request.use(
config =>{
const token = sessionStorage.getItem('token')
if (token) {
//后台给登录用户设置的token的键时什么，headers['''']里的键也应该保持一致
config.headers['token'] = `${token}`
}
return config
},
error =>{
return Promise.reject(error)
}
)
// 响应拦截器
axios.interceptors.response.use(res => {
    // 未设置状态码则默认成功状态
    const code = res.data.code|| 200;
	// console.log("code=",code);
    // 获取错误信息
    const msg = res.data.msg 
	//console.log("msg=",msg);
	const tokenStatus=res.data.sstatus//token状态
	if(tokenStatus){//如果存在
	console.log("tokenStatus=",tokenStatus);
	if(tokenStatus!=true){//token不正确，重新登录
		//alert("跳转")
		ElMessage({ message: msg, type: 'error' })
		sessionStorage.removeItem('token');
		sessionStorage.removeItem('isLogin');
		router.push('/');
		return Promise.reject(new Error(msg))
	}else{
		//token正确则放行
		return  Promise.resolve(res)
	}
	}

    if (code == 401) {
		// 401 清除token，并跳转到登录页面
	  sessionStorage.removeItem('token');
	  sessionStorage.removeItem('isLogin');
	  router.push('/');
      return Promise.reject('无效的会话，或者会话已过期，请重新登录。')
    } else if (code == 500) {
      ElMessage({ message: msg, type: 'error' })
      return Promise.reject(new Error(msg))
    } else if (code == 601) {
      ElMessage({ message: msg, type: 'warning' })
      return Promise.reject(new Error(msg))
    } else if (code != 200) {
      ElNotification.error({ title: msg })
      return Promise.reject('error')
    } else {//其他情况， 包括登录的响应会走这里
      return  Promise.resolve(res)
    }
  },
  error => {
    console.log('err' + error)
    // ElMessage({ message: message, type: 'error', duration: 5 * 1000 })
    return Promise.reject(error)
  }
)	   
	   
	   
	   
	



export default axios