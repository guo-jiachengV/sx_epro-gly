import axios from 'axios';

const api = axios.create({
  baseURL: 'http://locahost:8086', // 替换为你的后端URL
  timeout: 10000,
});

export default api;
